const api = 'https://api.themoviedb.org/3/';
const api_key = '5633e9213ebc88cd1df51e434512f7da';

/** 
*   get popular movies from specific page.
*   @param {number} page number of the page that you want to get the popular movie
*   @returns {Array} one array movie objects  
*/
async function getPopularMovies(page) {
    const reqStr = `${api}movie/popular?api_key=${api_key}&language=en-US&page=${page}`;
    const reponse = await fetch(reqStr);
    const movies = await reponse.json();
    return movies;
}
async function getReviewMovie(id,page=1) {
    const reqStr = `${api}movie/${id}/reviews?api_key=${api_key}&language=en-US&page=${page}`;
    const reponse = await fetch(reqStr);
    const review = await reponse.json();
    return review.results;
}
/** 
*   get length of a specific movie.
*   @param {number} id of the movie
*   @returns {object} {"hour":hour,"minute":minute} 
*/
async function getLengthMovie(id) {
    const reqStr = `${api}movie/${id}?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const movie = await reponse.json();
    let m = movie.runtime % 60;
    let h = (movie.runtime - m) / 60;
    return { "hour": h, "minute": m }
}
/** 
*   get cast and crew of a specific movie.
*   @param {number} id of the movie
*   @returns {JSON} ["id":id,"cast":[{Object},{Object},...],"crew":[{Object},{Object},...]] 
*/
async function getCreditsMovie(id) {
    const reqStr = `${api}movie/${id}/credits?api_key=${api_key}`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   get information of a specific person.
*   @param {number} person_id of the person
*   @returns {JSON} that hold all information 
*/
async function getPerson(person_id) {
    const reqStr = `${api}person/${person_id}?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   Get the movie credits for a person.
*   @param {number} person_id of the person
*   @returns {JSON} that hold all information 
*/
async function getMovieCredits(person_id) {
    const reqStr = `${api}person/${person_id}/movie_credits?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const result = await reponse.json();
    return result;
}
/** 
*   get director of specific movie in the crew.
*   @param {Array} crew of the movie
*   @returns {Array} [] array hold all director of the movie 
*/
function getDirectorMovie(crew) {
    let results = [];
    for (let i of crew) {
        if (i.job == "Director") {
            results.push(i);
        }
    }
    return results;
}

/** 
*   loading the popular movies.
*/
async function loading() {
    document.title = "Popular Movies";
    $('#main').empty();
    addLoading('main');
    let movie;
    let vars=getUrlVars();
    let page_number = vars.page == undefined ? 1 : parseInt(vars.page);
    if(vars.search){
        $('#mainTitle').html(`Results of keyword  "${vars.search.replace(/\+/g,' ')}"`);
        movie =await searchMovie(vars.search.replace(/\+/g,' '),page_number);
        reloadPagination(page_number,movie.total_pages,"search",vars.search);
    }
    else{               
        $('#mainTitle').html('Popular Movies');
        movie = await getPopularMovies(page_number);  
        reloadPagination(page_number,movie.total_pages);      
    }    
    for (let i of movie.results) {
        await addOverviewMovie(i,'main');
    }    
    removeLoading('main');
}

/** 
*   reaload the pagination of website.
*   @param {number}page_number page number of current website
*/
function reloadPagination(page_number,total_page,tag='',tag_var='') {
    let strVars='';
    if(tag!='' && tag_var!=''){
        strVars=strVars+'&'+tag+'='+tag_var;
    }
    if(((total_page-5)>=0)&&(page_number>=(total_page-5))){
        $('#main').append(`
        <div class= "col-12 d-flex justify-center">
        <nav class="mx-auto" aria-label="...">
        <ul class="pagination">
            <li class="page-item">
              <a class="page-link" id="previous" href="?page=${page_number-1}${strVars}"" tabindex="-1">Previous</a>
            </li>        
            <a class="page-link"  href="?page=${total_page - 5}${strVars}">${total_page - 5}</a></li>
            <li class="page-item">
            <a class="page-link"  href="?page=${total_page - 4}${strVars}">${total_page - 4}</a></li>
            <li class="page-item">
            <a class="page-link"  href="?page=${total_page - 3}${strVars}">${total_page-3}</a></li>
            <li class="page-item">
        <a class="page-link"  href="?page=${total_page - 2}${strVars}">${total_page - 2}</a></li>
        <li class="page-item">
        <a class="page-link"  href="?page=${total_page - 1}${strVars}">${total_page - 1}</a></li>
        <li class="page-item">
        <a class="page-link"  href="?page=${total_page}${strVars}">${total_page}</a></li>  
    <li class="page-item disabled" id="next">
              <a class="page-link "  href="?page=${total_page}${strVars}">Next</a>
            </li>
        </ul>
      </nav> </div>`);
    }
    else if(total_page>0&&(total_page-5)<0){
        let items='';
        for(let i=1;i<=total_page;i++){
            items=items+` <li class="page-item">
            <a class="page-link"  href="?page=${i}${strVars}">${i}</a></li>  `
        }
        $('#main').append(`
        <div class= "col-12 d-flex justify-center">
        <nav class="mx-auto" aria-label="...">
        <ul class="pagination">               
            ${items}   
            
        </ul>
      </nav> </div>`);

    }else if(total_page){
    $('#main').append(`
    <div class= "col-12 d-flex justify-center">
    <nav class="mx-auto" aria-label="...">
    <ul class="pagination">
        <li class="page-item" id="previous" >
          <a class="page-link" href="?page=${page_number-1}${strVars}"" tabindex="-1">Previous</a>
        </li>        
        <li class="page-item">
           <a class="page-link"  href="?page=${page_number}${strVars}">${page_number}</a>
        </li>
        <li class="page-item " aria-current="page">
          <a class="page-link"  href="?page=${page_number+1}${strVars}">${page_number+1}</a>
        </li>
        <li class="page-item">
          <a class="page-link"  href="?page=${page_number + 2}${strVars}">${page_number + 2}</a></li>
        <li class="page-item">
        <li class="page-item" id="threepoint">
        <a class="page-link"  >...</a></li>
        <li class="page-item">
    <a class="page-link"  href="?page=${total_page - 2}${strVars}">${total_page - 2}</a></li>
    <li class="page-item">
    <a class="page-link"  href="?page=${total_page - 1}${strVars}">${total_page - 1}</a></li>
    <li class="page-item">
    <a class="page-link"  href="?page=${total_page - 1}${strVars}">${total_page}</a></li>  
<li class="page-item">
          <a class="page-link" id="next" href="?page=${page_number+1}${strVars}">Next</a>
        </li>
    </ul>
  </nav> </div>`);
  if(page_number==1){
      $('#previous').addClass("disabled");
  }
}
else{
    $('#main').append(`<h1 class="text-center">NO RESULT</h1>`)
}
}

/** 
*   add an movie overview to the popular movies
*   @param {object} movie overview information of an movie
*/
async function addOverviewMovie(movie,div_id) {
    let len = await getLengthMovie(movie.id);
    $(`#${div_id}`).append(`
    <div class=" mb-3 col-3 px-2">
        <div class="card shadow h-100" onclick="window.location.href='./detail.html?id=${movie.id}&tag=movie'" style="hover:cursor" >
           <div class="row no-gutters">
              <div class="col-md-5">
                 <img src="https://image.tmdb.org/t/p/w185_and_h278_bestv2${movie.poster_path}" class="card-img" alt="...">
              </div>
              <div class="col-md-7">
                    <div class="card-body">
                        <h5 class="card-title"><b>${movie.title}</b></h5>
                        <p class="card-text overview"><small>${standOverview(movie.overview)}</small></p>
                        <small><b>Updated:</b> ${movie.release_date}</small><br/>
                        <small><b>Length:</b> ${len.hour}h ${len.minute}m</small><br/>
                        <div class="row col">
                           <p class='d-block mb-0'><small><b>Rating:</b></small></p>
                           <div class="col-9">
                               <div class="progress">
                                  <div class="progress-bar bg-success" role="progressbar" style="width: ${movie.vote_average * 10}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">${movie.vote_average * 10}%</div>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
`)
}


/** 
*   add loading animation while wesite is loading.
*   @param {string}id  id of block that you want to add loading animation
*/
function addLoading(id) {

    $(`#${id}`).append(`
    <div class="position-absolute bg-white " id="${id}Load" style="width:100%;height:100%;top:0;left:0;z-index:100">
        <div class="d-flex justify-content-center">
            <div class="spinner-border" role="status">
              <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>`);
}

/** 
*   remove loading animation when website loaded.
*   @param {string}id  id of block that you want to remove loading animation
*/
function removeLoading(id) {
    $(`#${id}Load`).remove();
}
/** 
*   limit the overview string.
*   @param {string}overview  original overview string
*   @returns {string} the string that is limited
*/
function standOverview(overview) {
    let strIndex = overview.indexOf(' ', 200);
    if (strIndex) {
        return overview.slice(0, strIndex) + "...";
    }
    return overview;
}
/**
 * get all variables in currently URL
 * @returns {Object} all variables in URL
 */
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
/**
 * format time of movie to get day,month,year from it
 * @param {string} time 
 * @returns {Object} { "year": year, "month": month, "day": day }
 */
function formatTime(time) {
    let [_, year, month, day] = /(\d{4})-(\d{1,2})-(\d{1,2})/.exec(time);
    switch (month) {
        case '1':
            month = "January";
            break;
        case '2':
            month = "February";
            break;
        case '3':
            month = "March";
            break;
        case '4':
            month = "April";
            break;
        case '5':
            month = "May";
            break;
        case '6':
            month = "June";
            break;
        case '7':
            month = "July";
            break;
        case '8':
            month = "August";
            break;
        case '9':
            month = "September";
            break;
        case '10':
            month = "October";
            break;
        case '11':
            month = "November";
            break;
        case '12':
            month = "December";
            break;
    }

    return { "year": year, "month": month, "day": day };
}
/**
 * format the runtime of the movie
 * @param {number} time
 * @return {Object} { "hour": h, "minute": m }
 */
function formatLengthMovie(time) {
    let m = time % 60;
    let h = (time - m) / 60;
    return { "hour": h, "minute": m }
}
/**
 * add top 6 billed cast of the movie
 * @param {Array} cast all cast of the movie 
 */
function addTopCast(cast) {
    for (let i of cast) {
        $('#cast').append(`
            <div onclick="window.location.href='./detail.html?id=${i.id}&tag=cast'" class="col-2 px-1">
                <div class="card shadow">
                    <img src="https://image.tmdb.org/t/p/w138_and_h175_face/${i.profile_path}" class="card-img-top" alt="...">
                    <div class="card-body">
                      <h5 class="card-title"><b>${i.name}</b></h5>
                      <p class="card-text">${i.character}</p>              
                    </div>
                </div>
            </div>`);
    }

}
/**
 * add trailer from youtube of the movie
 * @param {Array} trailer all trailer object of the movie
 */
function addTrailer(trailer) {
    for (let i of trailer) {
        if (i.site == 'YouTube') {
            $('#trailer').append(`
            <iframe title="YouTube video player" class="youtube-player mx-auto" type="text/html" 
             width="640" height="390" src="http://www.youtube.com/embed/${i.key}"
             frameborder="0" allowFullScreen></iframe>
            `);
            break;
        }
    }
}
async function getDetailMovie(id){
    const reqStr = `${api}movie/${id}?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const movie = await reponse.json();
    return movie;
}
async function getTrailerMovie(id){
    const reqStr = `${api}movie/${id}/videos?api_key=${api_key}&language=en-US`;
    const reponse = await fetch(reqStr);
    const trailer = await reponse.json();
    return trailer.results;
}
async function searchMovie(keyword,page=1){
    let searchKeyword=  keyword.replace('+',' ');
    const reqStr = `${api}search/multi?api_key=${api_key}&language=en-US&page=${page}&include_adult=true&query=${searchKeyword}`;
    const reponse = await fetch(reqStr);
    const search = await reponse.json();
    const result={"total_pages":search.total_pages};
    let results=[];
    for (let i of search.results){
        if(i.media_type=="movie") 
        {
            results.push(i);
            continue;
        };
        if(i.known_for!=null){
            for(let j of i.known_for)
            {
                if(j.media_type=="movie") results.push(j);
            }
        }        
    }
    result["results"]=results;
    return result;
}

/**
 * loadDetail of the movie
 */
async function loadDetail() {
    let vars = getUrlVars();
    $('#main').empty();
    addLoading('main');
    if(vars.tag=='movie'){
        const movie = await getDetailMovie(vars.id);
        const credits = await getCreditsMovie(vars.id);
        const casts = credits.cast;   
        const trailers = await getTrailerMovie(vars.id);
        const reviews=await getReviewMovie(vars.id);
        const director =  getDirectorMovie(credits.crew);
        let  strDirector='';
        for (let j of director){
            strDirector=strDirector+j.name+'; ';
        }
        let genres='';
        for (let j of movie.genres){
            genres=genres+ j.name+';'; 
        }
        document.title = `${movie.original_title}`;
        let release_date = formatTime(movie.release_date);
        let lenghtMovie = formatLengthMovie(movie.runtime);
        $('#main').append(
            `<div class=" mb-3 col-6 px-2 mx-auto">
                <div class="card shadow h-100" >
                    <div class="row no-gutters">
                        <div class="col-md-5">
                           <img src="https://image.tmdb.org/t/p/w300_and_h450_bestv2${movie.poster_path}" class="card-img" alt="...">
                        </div>
                        <div class="col-md-7">
                            <div class="card-body">     
                                <h4 class="card-title"><b>${movie.original_title} (${release_date.year})</b></h4>
                                <p class="card-text">${movie.overview}</p>
                                
                                <small><b>Updated:</b> ${release_date.month} ${release_date.day} ${release_date.year}</small><br/>
                                <small><b>Length:</b> ${lenghtMovie.hour}h ${lenghtMovie.minute}m</small><br/>
                                <small><b>Director:</b> ${strDirector}</small><br/>
                                <small><b>Genres:</b> ${genres}</small>
                                <div class="row col">
                                <p class='d-block mb-0'><small><b>Rating:</b></small></p>
                                <div class="col-9">
                                  <div class="progress">
                                      <div class="progress-bar bg-success" role="progressbar" style="width: ${movie.vote_average * 10}%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">${movie.vote_average * 10}%</div>
                                  </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`
        )
        $('#review').append(`<h4 class="col-12">REVIEWS</h4>`)
        for(let j of reviews){
            $('#review').append(`
            <div class="card">
                <div class="card-header">
                  ${j.author}
                </div>
                <div class="card-body">                  
                  <p class="card-text">${j.content}</p>                 
                </div>
            </div>`)
        }
        $('#cast').append(`<h4  class="col-12">CAST</h4>`)
        addTopCast(casts);
    }   
    else if(vars.tag=="cast"){
        const person = await getPerson(vars.id);
        const movies =  await getMovieCredits(vars.id)        
        document.title = `${person.name}`;
        $('#main').append(
            `<div class=" mb-3 col-6 px-2 mx-auto">
                <div class="card shadow h-100" >
                    <div class="row no-gutters">
                        <div class="col-md-5">
                           <img src="https://image.tmdb.org/t/p/w300_and_h450_bestv2${person.profile_path}" class="card-img" alt="...">
                        </div>
                        <div class="col-md-7">
                            <div class="card-body">     
                                <h4 class="card-title"><b>${person.name}</b></h4>
                                <p class="card-text">${person.biography}</p>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>`); 
        $('#movie').append(`<h4  class="col-12">MOVIES</h4>`)
        for(let i of movies.cast){
            console.log(i);
            addOverviewMovie(i,'movie');
        }          
    }
    removeLoading('main');
}